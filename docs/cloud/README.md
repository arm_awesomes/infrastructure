# Cloud

## Domestic

国内云计算概念的引入大约，而兴起大约是以2009年“云计算中国论坛”为标志

## Short History of Cloud

1. 2006： `Google`推出`Cloud Computing`概念

## Cloud in Advertising

1. `IaaS`
    1. `AWS(Amazon Web Services)`
        1. 2005： `S3`
        1. 2006： `EC2`
        1. 2008： `EC2`正式商用
1. `PaaS`
    1. `GAE(Google App Engine)`
        1. 2008/04： 试商用
        1. 2009/02： 正式商用
1. `SaaS`
    1. `Salesforce` ：
        1. 2004： `CRM`软件租赁服务

## Cloud Platform vs Cloud Computing

