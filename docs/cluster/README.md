# Cluster

## What is Cluster

集群： 指一组服务器，通过彼此间协同工作，对外共同提供一个的服务或应用程序，以提升服务或应用程序的可用性(availability)、可靠性(reliability)，以及可扩展性(scalability)。当集群内的一个服务器无法工作时，该服务器的工作将会转移给其它的服务器，以提供不间断的服务。

## Windows Server Cluster

Windows Server支持了三种集群技术，分别是NLB、CLB与MSCS，其中，NLB和MSCS已经集成在Windows Server中，而CLB则需要购买Application Center。

1. NLB(Network Load Balance，网络负载均衡)，属于前端的集群技术，提供以TCP/IP为基础的服务与应用程序的网络流量负载均衡，用于提升操作系统的可用性和延展性。常见的应用有terminal service、web service与web application等。NLB是通过一个虚拟的IP提供对外服务。当收到请求时，NLB会随机决定由哪一台服务器来处理这个请求。
2. CLB(Component Load Balance，组件负载均衡)，属于中间层的集群技术，提供使用COM+组件的中介层应用程序的动态负载均衡，用于提升系统的可用性和延展性。CLB会依据目前的工作负载来决定由谁来处理服务请求。
3. MSCS(Microsoft Cluster Server，Windows服务器集群)，属于后端的集群技术，提供后端服务与应用程序的容错移转(failover)，用来提升系统的可用性。常见的应用有SQL Server与Exchange Server等。MSCS是由client来决定由谁来处理服务请求，所有服务器共享一个share storage来储存session状态。当主动服务器挂了，则继续由被动服务器接手。被动服务器会从share storage取出session状态，继续未完成的工作，以达到容错移转的目的。

### NLB的运作原理

对Windows Server集群有了基本的了解之后，我们来进一步了解NLB是怎么样将Client的服务请求分配给丛集内的服务器的。NLB是使用筛选模式相似性算法来分配服务请求。它又分成无相似性、单一相似性，和class C等三种，简单介绍如下：

1. 无相似性： Client的服务请求会平均分配到丛集内的每一部服务器。假设NLB丛集内有2部服务器。当接到Client的请求时，NLB会将第1个请求交由第1部服务器来处理，第2个请求交由第2部服务器来处理，第3个请求交由第1部服务器来处理，…依此类推。所有Client联机会平均分配到每一部服务器，因此可以达到最佳的负载平衡。如果需要执行交易处理，为了能够共享session状态，则必须将session状态集中储存在state或database server中。这种方式适用于大部分的应用程序。
2. 单一相似性： Client的服务请求会固定分配到丛集内的某一部服务器。当接到Client的请求时，NLB会根据Client 的IP来决定交由哪一部服务器来处理，也就是一部服务器只会处理来自某些IP的请求。因为一个IP的服务请求只会固定由一个服务器来处理，因此没有session状态共享的问题，但可能会导致负载不平衡。这种方式适用于联机需支持 SSL 集多重联机的通讯协议 (例如FTP与PPTP等)。
3. Class C： 与单一相似性类似，不同的是Class C是根据IP的Class C屏蔽来决定交由哪一部服务器来处理，也就是一部服务器只会处理来自某些网段C的请求。这种方式可确保使用多重 Proxy 的客户端能导向到相同的服务器。

结论： 在建立NLB之前，有一些限制条件，例如服务器的Private IP必须是固定的，并且在同一个网段，如果只有一张网卡，则集群内的服务器之间无法互通，因此必须装配两张网卡，一张网卡用于建立NLB，另一张网卡则用于服务器之间的heart beat，用以侦测集群内服务器是否正常运作。如果同时使用两张网卡，则只能在其中一张网卡上设定default gateway，否则封包将无法转送至正确的IP地址上。

## Bibliographies

1. Windows Server集群(cluster)技术揭秘 <https://blog.51cto.com/jasonliping/1382749>