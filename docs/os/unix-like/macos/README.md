# MacOS

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->


## Virtual Memory

```bash {.line-numbers}
# check virtual memory
# or through "Activity Monitor" APP
vm_stat

# swap files location
/private/var/vm/

# disable virtual memory and delete swap files
sudo launchctl unload -w \
    /System/Library/LaunchDaemons/com.apple.dynamic_pager.plist
sudo rm /private/var/vm/swapfile*
```
