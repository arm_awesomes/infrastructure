# CentOS

## yum

### EPEL

`EPEL(Extra Packages for Enterprise Linux)`由`Fedora`社区打造，为`RHEL`及其衍生发行版如`CentOS`、`Scientific Linux`等`distro`提供高质量软件包的项目。

```bash {.line-numbers}
# will download epel.repo and epel-testing.repo
# into /etc/yum.repo.d/
yum install -y epel-release
```