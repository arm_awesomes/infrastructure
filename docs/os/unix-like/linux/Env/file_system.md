# Linux File System

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [UNIX-like Directory Structure](#unix-like-directory-structure)
    1. [Linux/FHS](#linuxfhs)
2. [file type](#file-type)

<!-- /code_chunk_output -->

## UNIX-like Directory Structure

### Linux FHS

`FHS(Filesystem Hierarchy Standard)`

```bash{.line-numbers}
\
|-- bin ->  usr/bin     ==> BINary
|-- dev     ==> DEVice
|-- etc     ==> Editable Text Configuration/ETCetra/Extended Tool Chest
|-- home
|-- lib -> usr/lib
|-- lib64 -> usr/lib64
|-- lost+found
|-- media
|-- mnt     ==> MouNT
|-- misc
|-- opt     ==> OPTional
|-- proc    ==> PROCess
|-- root
|-- run
|-- sbin -> usr/sbin        ==> Super user BINary
|-- srv     ==> SeRVer
|-- sys
|-- tmp     ==> TeMPorary
|-- usr     ==> Unix System Resources
|-- var
```

> **see more:**
> [Filesystem Hierarchy Standard](http://refspecs.linuxfoundation.org/fhs.shtml)

/	根目录，只能包含目录，不能包含具体文件。
/bin	存放可执行文件。很多命令就对应/bin目录下的某个程序，例如 ls、cp、mkdir。/bin目录对所有用户有效。
/dev	硬件驱动程序。例如声卡、磁盘驱动等，还有如 /dev/null、/dev/console、/dev/zero、/dev/full 等文件。
/etc	主要包含系统配置文件和用户、用户组配置文件。
/lib	主要包含共享库文件，类似于Windows下的DLL；有时也会包含内核相关文件。
/boot	系统启动文件，例如Linux内核、引导程序等。
/home	用户工作目录（主目录），每个用户都会分配一个目录。
/mnt	临时挂载文件系统。这个目录一般是用于存放挂载储存设备的挂载目录的，例如挂载CD-ROM的cdrom目录。
/proc	操作系统运行时，进程（正在运行中的程序）信息及内核信息（比如cpu、硬盘分区、内存信息等）存放在这里。/proc目录伪装的文件系统proc的挂载目录，proc并不是真正的文件系统。
/tmp	临时文件目录，系统重启后不会被保存。
/usr	/user目下的文件比较混杂，包含了管理命令、共享文件、库文件等，可以被很多用户使用。
/var	主要包含一些可变长度的文件，会经常对数据进行读写，例如日志文件和打印队列里的文件。
/sbin	和 /bin 类似，主要包含可执行文件，不过一般是系统管理所需要的，不是所有用户都需要。
1、/bin ：获得最小的系统可操作性所需要的命令
2、/boot ：内核和加载内核所需的文件
3、/dev ：终端、磁盘、调制解调器等的设备项
4、/etc ：关键的启动文件和配置文件
5、/home ：用户的主目录
6、/lib ：C编译器的库和部分C编译器
7、/media ：可移动介质上文件系统的安装点
8、/opt ：可选的应用安装包
9、/proc ：所有正在运行进程的映像
10、/root ：超级用户的主目录
11、/sbin ：引导、修复或者恢复系统的命令
12、/tmp ：每次重新引导就消失的临时文件
13、/usr ：次要文件和命令的层次结构
14、/usr/bin ：大多数命令和可执行文件
15、/usr/include ：编译C程序的头文件
16、/usr/lib ：库，供标准程序使用的支持文件
17、/usr/local ：本地软件（用户所编写或者安装的软件）
18、/usr/local/bin ：本地的可执行文件
19、/usr/local/etc ：本地系统配置文件和命令
20、/usr/local/lib ：本地的支持文件
21、/usr/local/sbin ：静态链接的本地系统维护命令
22、/usr/local/src ：/usr/local/*的源代码
23、/usr/man ：联机用户手册
24、/usr/sbin不太关键的系统管理命令和修复命令
25、/usr/share ：多种系统共同的东西（只读）
26、/usr/share/man ：练级用户手册
27、/usr/src ：非本地软件包的源代码
28、/var ：系统专用数据和配置文件
29、/var/adm ：各种不同的东西
30、/var/log ：各种系统日志文件
31、/var/spool ：供打印机、邮件等使用的假脱机目录
32、/var/tmp ：更多的临时空间（在重新引导之后，文件予以保留）

```bash
/
├── Applications
├── Library
├── System
├── Users
├── Volumes
├── bin
├── cores
├── dev
├── etc -> private/etc
├── home -> /System/Volumes/Data/home
├── opt
├── private
├── sbin
├── tmp -> private/tmp
├── usr
└── var -> private/var
```

## file type

