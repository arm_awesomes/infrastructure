# Job and Task in Linux

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [Job Control Commands](#job-control-commands)

<!-- /code_chunk_output -->

## Job Control Commands

```bash{.line-numbers}
#print all current tty settings in human-readable form
stty -a
```
