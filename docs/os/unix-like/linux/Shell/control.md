# 流程控制

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

## if

```bash {.line-numbers}
if condition; then
    command
    ...
    command
elif condition; then
    command
    ...
    command
else
    command
    ...
    command
fi
```