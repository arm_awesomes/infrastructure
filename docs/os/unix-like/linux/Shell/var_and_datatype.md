# 变量与数据类型

## boolean

`Bash`有`test expression`，但没有`boolean`类型，即可以在`Bash`中声明和比较`string`和`integer`，在`Bash`中出现的`true`或`false`，要么是一个`string`，要么是一个`builtin command`（返回退出代码）。

`if true; then ...`实际上是`if <command>; then ...`，当`<command>`返回的退出代码为`0`时条件为`true`。

在`if <test expression>`这种形式中使用`true`或`false`时，实际上只是使用`"true"`或`"false"`的`string`，并且它们都是`non-empty string`。

```bash {.line-numbers, cmd}

if [[ false ]]; then
    echo "1st false is true"
else
    echo "1st false is false"
fi

if [[ "false" ]]; then
    echo "2nd false is true"
else
    echo "2nd false is false"
fi
```