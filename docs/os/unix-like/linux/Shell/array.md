# Array

```bash{.line-numbers}
if [[ " ${array[@]} " =~ " ${value} " ]]; then
    # whatever you want to do when arr contains value
fi
------------------------------------------------------
if [[ ! " ${array[@]} " =~ " ${value} " ]]; then
    # whatever you want to do when arr doesn't contain value
fi
------------------------------------------------------
if [[ " ${!array[@]} " =~ " ${value} " ]]; then
    # whatever you want to do when arr contains value
fi
------------------------------------------------------
${!array[@]}  获取所有的key
${array[@]}  获取所有的value
------------------------------------------------------
Indexed arrays use positive integer numbers as keys.
Shell 的 Array 数组 使用正整数作为key
                 --------------------
http://wiki.bash-hackers.org/syntax/arrays

if echo "${ARR[@]}" | grep -w "item_1" &>/dev/null; then
    echo "Found"
fi

array=()
declare -a ARRAY	
直接声明的数组不支持 字符串做为索引，只支持正整数作为key。

如要要声明 关系型数组，需要使用
declare -A ARRAY	
declare -A ARRAY	Declares an associative array ARRAY. This is the one and only way to create associative arrays.

------------------------------------------------------
```