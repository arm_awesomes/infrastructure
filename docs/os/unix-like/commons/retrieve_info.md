# Retrieve Information

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

## System Version Information

1. **kernel version** : 
    1. **`UNIX-Like`** : `uname`
    1. **`Linux`** : `cat /proc/version`
1. **distro version** :
    1. **`Linux`** : `lsb_release` (`rpm-based` need to manual install `redhat-lsb-core`)
    1. **`rpm-based`** : `cat /etc/redhat-release`
