# Lightweight Markup Language

## Widely Used LML

1. `AsciiDoc`
1. `Markdown`
1. `mediawiki`
1. `restructured text`

>[Lightweight Markup: Markdown, reStructuredText, MediaWiki, AsciiDoc, Org-mode](https://hyperpolyglot.org/lightweight-markup)

## Futhermore

1. [Lightweight markup language](https://en.wikipedia.org/wiki/Lightweight_markup_language)
